<?php

/**
 * @file
 * Provide views data.
 */

/**
 * Implements hook_field_views_data_alter().
 *
 * Handles standard entities. This is a nasty approach but Views ignores the
 * plugin ID when creating the actual handler object. It assumes there is
 * only one handler suitable for a particular table/field combination.
 * This is an issue only for relationships because of the way this is handled.
 * The 'standard' handler class is replaced with the Twig SQL handler.
 *
 * This approach does have the advantage the removal of the module
 * causes the relationship support to revert back to 'standard'.
 */
function views_sql_twig_relationship_field_views_data_alter(array &$data) {
  // Replace regular relationships.
  foreach ($data as $table_name => $table) {
    foreach ($table as $field_name => $type) {
      foreach ($type as $id => $value) {
        if (('relationship' == $id) && ('standard' == $value['id'])) {
          $data[$table_name][$field_name]['relationship']['id'] = 'views_sql_twig_fields_relationship';
        }
      }
    }
  }

  // Replace entity reverse relationships.
  foreach ($data as $table_name => $table) {
    foreach ($table as $field_name => $type) {
      foreach ($type as $id => $value) {
        if (('relationship' == $id) && ('entity_reverse' == $value['id'])) {
          $data[$table_name][$field_name]['relationship']['id'] = 'views_sql_twig_fields_entity_reverse';
        }
      }
    }
  }
}
