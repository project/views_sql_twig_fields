<?php

namespace Drupal\views_sql_twig_relationship\Plugin\views\join;

use Drupal\views\Plugin\views\join\JoinPluginBase;

/**
 * Provides support for raw SQL join condition.
 *
 * Construction should include configuration['sql_condition']
 *
 * @ingroup views_join_handlers
 *
 * @ViewsJoin("twig_sql_join")
 */
class TwigSqlJoinPlugin extends JoinPluginBase {

  /**
   * Builds the SQL for the join this object represents.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $select_query
   *   The select query object.
   * @param string $table
   *   The base table to join.
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $view_query
   *   The source views query.
   */
  public function buildJoin($select_query, $table, $view_query) {
    if (empty($this->configuration['table formula'])) {
      $right_table = $this->table;
    }
    else {
      $right_table = $this->configuration['table formula'];
    }

    if ($this->leftTable) {
      $left_table = $view_query->getTableInfo($this->leftTable);
      $left_field = $this->leftFormula ?: "$left_table[alias].$this->leftField";
    }
    else {
      // This can be used if left_field is a formula or something.
      // It should be used only *very* rarely.
      $left_field = $this->leftField;
      $left_table = NULL;
    }

    $condition = empty($this->configuration['sql_condition'])
      ? "$left_field " . $this->configuration['operator'] . " $table[alias].$this->field"
      : $this->configuration['sql_condition'];

    $arguments = [];

    // Tack on the extra.
    if (isset($this->extra)) {
      $this->joinAddExtra($arguments, $condition, $table, $select_query, $left_table);
    }

    $select_query->addJoin($this->type, $right_table, $table['alias'], $condition, $arguments);
  }

}
