<?php

namespace Drupal\views_sql_twig_relationship\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\relationship\Standard;
use Drupal\views_sql_twig_fields\TwigSqlFields;
use Drupal\views\Views;

/**
 * SQL Twig relationship handler .
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("views_sql_twig_fields_relationship")
 */
class ViewsSqlTwigFieldsRelationship extends Standard {
  use TwigSqlFields {
    submitOptionsForm as twigSqlSubmitOptionsForm;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $this->twigSqlDefineOptions($options);

    $options['enable_twig_sql'] = ['default' => 0];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsform($form, $form_state);

    $form['enable_twig_sql'] = [
      '#type' => "checkbox",
      '#title' => t('Enable Twig SQL'),
      '#description' => t('Twig SQL support will be added or removed based on this setting.'),
      '#default_value' => $this->options['enable_twig_sql'],
      '#weight' => -100,
      "#disabled" => $this->notAdmin(),
    ];

    if (!$this->options['enable_twig_sql']) {
      return;
    }

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    $this->twigSqlSubmitOptionsForm($form, $form_state);

    $this->options['enable_twig_sql'] =
      $form_state->getValue(['options', 'enable_twig_sql']);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->options['enable_twig_sql']) {
      $this->ensureMyTable();
      $this->regenerateSqlQuery();
      $def = $this->definition;

      // Figure out what base table this relationship brings to the party.
      $table_data = Views::viewsData()->get($this->definition['base']);
      $base_field = empty($this->definition['base field']) ? $table_data['table']['base']['field'] : $this->definition['base field'];

      $def['sql_condition'] = $this->options['sql'];
      $def['table'] = $this->definition['base'];
      $def['field'] = $base_field;
      $def['left_table'] = $this->tableAlias;
      $def['left_field'] = $this->realField;
      $def['adjusted'] = TRUE;

      if (!empty($this->options['required'])) {
        $def['type'] = 'INNER';
      }

      if (!empty($this->definition['extra'])) {
        $def['extra'] = $this->definition['extra'];
      }

      $join = Views::pluginManager('join')->createInstance('twig_sql_join', $def);

      // Use a short alias for this:
      $alias = $def['table'] . '_' . $this->table;

      $this->alias = $this->query->addRelationship($alias, $join, $this->definition['base'], $this->relationship);

      // Add access tags if the base table provide it.
      if (empty($this->query->options['disable_sql_rewrite']) && isset($table_data['table']['base']['access query tag'])) {
        $access_tag = $table_data['table']['base']['access query tag'];
        $this->query->addTag($access_tag);
      }
    }
    else {
      parent::query();
    }
  }

}
