<?php

namespace Drupal\views_sql_twig_relationship\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\relationship\EntityReverse;
use Drupal\views_sql_twig_fields\TwigSqlFields;
use Drupal\views\Views;

/**
 * SQL Twig relationship entity reverse handler .
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("views_sql_twig_fields_entity_reverse")
 */
class ViewsSqlTwigFieldsEntityReverse extends EntityReverse {
  use TwigSqlFields {
    submitOptionsForm as twigSqlSubmitOptionsForm;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $this->twigSqlDefineOptions($options);

    $options['enable_twig_sql'] = ['default' => 0];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsform($form, $form_state);

    $form['enable_twig_sql'] = [
      '#type' => "checkbox",
      '#title' => t('Enable Twig SQL'),
      '#description' => t('Twig SQL support will be added or removed based on this setting.'),
      '#default_value' => $this->options['enable_twig_sql'],
      '#weight' => -100,
      "#disabled" => $this->notAdmin(),
    ];

    if (!$this->options['enable_twig_sql']) {
      return;
    }

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    $this->twigSqlSubmitOptionsForm($form, $form_state);

    $this->options['enable_twig_sql'] =
      $form_state->getValue(['options', 'enable_twig_sql']);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->options['enable_twig_sql']) {
      $this->ensureMyTable();
      $def = $this->definition;
      $this->regenerateSqlQuery();

      // Figure out what base table this relationship brings to the party.
      $views_data = Views::viewsData()->get($this->table);
      $left_field = $views_data['table']['base']['field'];

      $first = [
        'sql_condition' => $this->options['sql'],
        'left_table' => $this->tableAlias,
        'left_field' => $left_field,
        'table' => $def['field table'],
        'field' => $def['field field'],
        'adjusted' => TRUE,
      ];

      if (!empty($this->options['required'])) {
        $firt['type'] = 'INNER';
      }

      if (!empty($this->definition['extra'])) {
        $first['extra'] = $def['extra'];
      }

      $first_join = $this->joinManager->createInstance('twig_sql_join', $first);

      $this->first_alias = $this->query->addTable($this->definition['field table'], $this->relationship, $first_join);

      // Second, relate the field table to the entity specified using
      // the entity id on the field table and the entity's id field.
      $second = [
        'left_table' => $this->first_alias,
        'left_field' => 'entity_id',
        'table' => $def['base'],
        'field' => $def['base field'],
        'adjusted' => TRUE,
      ];

      if (!empty($this->options['required'])) {
        $second['type'] = 'INNER';
      }

      $second_join = $this->joinManager->createInstance('twig_sql_join', $second);
      $second_join->adjusted = TRUE;

      // Use a short alias for this:
      $alias = $this->definition['field_name'] . '_' . $this->table;

      $this->alias = $this->query->addRelationship($alias, $second_join, $this->definition['base'], $this->relationship);
    }
    else {
      parent::query();
    }
  }

}
