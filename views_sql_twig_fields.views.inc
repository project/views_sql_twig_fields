<?php

/**
 * @file
 * Provide views data.
 */

/**
 * Implements hook_views_data().
 */
function views_sql_twig_fields_views_data() {
  $data['views']['sql_twig_fields_field'] = [
    'title' => t('SQL Twig field'),
    'help' => t('SQL field generated using a twig expression.'),
    'field' => [
      'id' => 'views_sql_twig_fields_field',
    ],
  ];

  $data['views']['views_sql_twig_fields_filter'] = [
    'title' => t('SQL Twig filter'),
    'help' => t('SQL filter generated using a twig expression.'),
    'filter' => [
      'id' => 'views_sql_twig_fields_filter',
    ],
  ];

  $data['views']['views_sql_twig_fields_sort'] = [
    'title' => t('SQL Twig sort'),
    'help' => t('SQL sort generated using a twig expression.'),
    'sort' => [
      'id' => 'views_sql_twig_fields_sort',
    ],
  ];

  $data['views']['views_sql_twig_fields_argument'] = [
    'title' => t('SQL Twig argument'),
    'help' => t('SQL argument generated using a twig expression.'),
    'argument' => [
      'id' => 'views_sql_twig_fields_argument',
    ],
  ];

  return $data;
}
