<?php

namespace Drupal\views_sql_twig_fields;

use Drupal\user\Entity\User;
use Drupal\views\Views;
use Drupal\Core\Form\FormStateInterface;

/**
 * Twig SQL field form support.
 */
trait TwigSqlFields {

  /**
   * Check permission.
   *
   * @return bool
   *   TRUE if not administrator.
   */
  protected function notAdmin() {
    return !\Drupal::currentUser()->hasPermission('administer twig sql');
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableGlobalTokens($prepared = FALSE, array $types = []) {
    return parent::getAvailableGlobalTokens($prepared, ['user', 'site', 'view']);
  }

  /**
   * Add options to the array.
   *
   * @param array $options
   *   List of options.
   */
  protected function twigSqlDefineOptions(&$options) {
    $options['sql'] = ['default' => '1'];
    $options['formula'] = ['default' => '1'];
    $options['variables'] = ['default' => ''];
    $options['mode'] = ['default' => 'always'];
    $options['my_group_type'] = ['default' => 'group'];
  }

  /**
   * Get form that lists fields in for each table.
   */
  protected function getSqlFieldForm() {
    $this->view->initQuery();
    $tables = [];
    foreach ($this->view->display_handler->getHandlers('relationship') as $relationship => $handler) {
      if ($handler->table != 'views') {
        $handler->query();
      }
    }

    // Generate form.
    $form = [];
    foreach ($this->view->query->getTableQueue() as $id => $row) {
      $table = $row['table'];
      $fields = [];

      foreach (\Drupal::database()->query("show columns from $table")->fetchAll() as $field) {
        $fields[] = "$table.$field->Field";
      }

      $form[] = [
        '#type' => 'details',
        '#title' => $id,
        '#open' => FALSE,
        0 => ['#markup' => implode('<br>', $fields) . ' ' . print_r($table, 1)],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function twigSqlBuildOptionsForm(&$form, FormStateInterface $form_state) {
    $this->view->initStyle();

    // Layout the form.
    $form['twig_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Twig settings'),
      '#open' => FALSE,
      '#weight' => -10,
    ];

    $form['twig_details']['twig_definition'] = [
      '#type' => 'details',
      '#title' => $this->t('Twig definitions'),
      '#open' => FALSE,
    ];

    $form['twig_details']['twig_definition']['twig_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Tables'),
      '#open' => FALSE,
      '#description' => $this->t('There are the currently accessible tables
        and fields. Copy and paste any of these to either the Twig variables
        or the Twig or SQL expression as needed.'
      ),
      0 => $this->getSqlFieldForm(),
    ];

    // Add global tokens.
    $this->globalTokenForm($form['twig_details']['twig_definition'], $form_state);

    $children = [];
    foreach ($this->view->display_handler->getHandlers('argument') as $arg => $handler) {
      $info = $handler->adminLabel();
      foreach ([
        'arguments' => 'title',
        'raw_arguments' => 'input',
      ] as $key => $suffix) {
        $children[] = "[$key:$arg] - $info $suffix";
      }
    }

    if ($children) {
      $form['twig_details']['twig_definition']['global_tokens']['list']['#items']['arguments'] = [
        '#markup' => 'arguments',
        'children' => $children,
      ];
    }

    $form['twig_details']['twig_definition']['variables'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twig substitutions'),
      '#description' => $this->t('Each line is of the form "search|replace". 
        All text matching <b>search</b> will be replaced by <b>replace</b>. 
        You can use search values like [my_var] or twig-style variables like 
        {{ foobar }}. The substition is done BEFORE twig evalution so the
        replcaement can be twig expressions that will be evaluated after
        the substitution. 
        '),
      '#rows' => 8,
      '#resizable' => TRUE,
      '#default_value' => $this->options['variables'],
      "#disabled" => $this->notAdmin(),
    ];

    // Include predefined values.
    $def = $this->definition;
    switch ($def['id']) {
      case 'views_sql_twig_fields_relationship':
        $form['twig_details']['twig_definition'][]['#markup'] = $this->t(
          'These variables are available from the relationship definition:');
        $form['twig_details']['twig_definition'][]['#markup'] =
          '<br>&nbsp; {{ left_table }}.{{ left_field }}' .
          '<br>&nbsp; {{ right_table }}.{{ right_field }}' .
          '<br>&nbsp; {{ table_alias }}';
        break;

      case 'views_sql_twig_fields_argument':
        $form['twig_details']['twig_definition'][]['#markup'] = $this->t(
          'The variableis {{ argument }} and {{ isnumeric }} will be available at runtime. 
          Be VERY CAREFUL about using the argument to avoid SQL injection attacks.  
          You can use a test like this:');
        $form['twig_details']['twig_definition'][]['#markup'] =
          '<br><b>{% if isnumeric %} {{ argument }} {% else %} "not a number" {% endif %} </b>';
        break;
    }

    $form['twig_details']['twig_definition']['formula'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twig expression'),
      '#description' => $this->t('This twig expression should evaluate to a valid SQL expression.
        It can use values from the Twig variables section.'),
      '#rows' => 8,
      '#resizable' => TRUE,
      '#default_value' => $this->options['formula'],
      "#disabled" => $this->notAdmin(),
    ];

    $form['twig_details']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Regenerate SQL from twig expression'),
      '#description' => $this->t('This determines how and when the SQL expression is generated
        Always regenerate is only needed if a twig expression needs to be regenerated when the SQL statement is executed.
        If you use another option them check your SQL expression after it is saved to make sure it is what you want.
        Use Raw SQL if you do not want the SQL field to change. '
      ),
      '#options' => [
        'always' => 'Always regenerate',
        'save' => 'Regenerate on save',
        'once' => 'Regenerate on this save',
        'runtime' => 'Regenerate at runtime only',
        'never' => 'Raw SQL (Never regenerate)' ,
      ],
      '#default_value' => $this->options['mode'],
      "#disabled" => $this->notAdmin(),
    ];

    $form['twig_details']['sql'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SQL expression'),
      '#description' => $this->t('This SQL expression will be used if Always regenerate is not specified.'),
      '#rows' => 8,
      '#resizable' => TRUE,
      '#default_value' => $this->options['sql'],
      "#disabled" => $this->notAdmin(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    $mode = $form_state->getValue(['options', 'twig_details', 'mode']);
    $this->options['mode'] = $mode;
    $this->options['sql'] = $form_state->getValue(
      ['options', 'twig_details', 'sql']);
    $this->options['variables'] = $form_state->getValue(
      ['options', 'twig_details', 'twig_definition', 'variables']);
    $this->options['formula'] = $form_state->getValue(
      ['options', 'twig_details', 'twig_definition', 'formula']);

    if (!('never' == $mode || 'runtime' == $mode)) {
      $this->regenerateSql();
      if ('once' == $mode) {
        $this->options['mode'] = 'never';
      }
    }
  }

  /**
   * Provide a form for aggregation settings.
   */
  public function buildGroupByForm(&$form, FormStateInterface $form_state) {
    parent::buildGroupByForm($form, $form_state);

    $options = [];
    foreach ($this->query->getAggregationInfo() as $key => $value) {
      $options[$key] = $value['title'];
    }

    unset($form['group_type']);

    $form['my_group_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Aggregation type'),
      '#description' => $this->t("Change this setting in the field configutation form."),
      '#options' => $options,
      '#parent' => NULL,
      '#default_value' => $this->options['my_group_type'],
    ];
  }

  /**
   * Regenerate SQL expression before a query.
   *
   * @param array $parameters
   *   Additional parameter  list [ variable => value, .. ].
   */
  protected function regenerateSqlQuery($parameters = []) {
    switch ($this->options['mode']) {
      case 'always':
      case 'runtime':
        $this->regenerateSql($parameters);
    }
  }

  /**
   * Regenerate SQL expression.
   *
   * @param array $parameters
   *   Additional parameter  list [ variable => value, .. ].
   */
  protected function regenerateSql($parameters = []) {
    $search = [];
    $replace = [];

    // Add global Views tokens.
    foreach ($this->getAvailableGlobalTokens() as $key1 => $group) {
      foreach ($group as $key2 => $value) {
        $search[] = '[' . $key1 . ':' . $key2 . ']';
        $replace[] = (string) $value['name'];
      }
    }

    // Generate user information.
    $id = \Drupal::currentUser()->id();
    $user = User::load($id);

    $search[] = '[user:uid]';
    $replace[] = $user->id();
    $search[] = '[user:name]';
    $replace[] = $user->getAccountName();
    $search[] = '[user:account-name]';
    $replace[] = $user->getAccountName();
    $search[] = '[user:display-name]';
    $replace[] = $user->getDisplayName();
    $search[] = '[user:mail]';
    $replace[] = $user->getEmail();
    $search[] = '[user:url]';
    $replace[] = "/user/$id";
    $search[] = '[user:edit-url]';
    $replace[] = "/user/$id/edit";
    $search[] = '[user:last-login]';
    $replace[] = $user->getLastAccessedTime();
    $search[] = '[user:created]';
    $replace[] = $user->getCreatedTime();

    // Generate path information.
    $path = (string) \Drupal::service('path.current')->getPath();
    $path_alias = (string) \Drupal::service('path_alias.manager')->getAliasByPath($path);
    $search[] = '[path:alias]';
    $replace[] = $path_alias;
    $search[] = '[path:raw]';
    $replace[] = $path;
    $search[] = '[path:route]';
    $replace[] = \Drupal::routeMatch()->getRouteName();
    $search[] = '[path:host]';
    $replace[] = \Drupal::request()->getHost();
    foreach (\Drupal::request()->query->all() as $key => $value) {
      if ('string' == gettype($value)) {
        $search[] = "[path:parameter:$key]";
        $replace[] = $value;
      }
    }

    // Add any arguments.
    foreach ($this->view->display_handler->getHandlers('argument') as $arg => $handler) {
      if ($this->view->argument) {
        $search[] = "[arguments:$arg]";
        $replace[] = (string) $handler->getTitle();
        $search[] = "[raw_arguments:$arg]";
        $replace[] = (string) $handler->getValue();
      }
      else {
        $search[] = "[arguments:$arg]";
        $replace[] = '';
        $search[] = "[raw_arguments:$arg]";
        $replace[] = '';
      }
    }

    // Add user defined substituion variables.
    $variables = $this->options['variables'];
    if ($variables) {
      foreach (explode("\r\n", $this->options['variables']) as $row) {
        if ($row) {
          $split = explode('|', $row);
          if (2 == count($split)) {
            $search[] = $split[0];
            $replace[] = $split[1];
          }
          else {
            \Drupal::messenger()->addError($this->t('Twig SQL substitution error:') . " '$row'");
          }
        }
      }
    }

    // Add default relationship fields.
    $def = $this->definition;
    if ('views_sql_twig_fields_relationship' == $def['id']) {
      $table_data = Views::viewsData()->get($def['base']);

      $parameters['right_table'] = $def['base'];
      $parameters['right_field'] = empty($def['base field'])
        ? $table_data['table']['base']['field']
        : $def['base field'];
      $parameters['left_table'] = $this->tableAlias;
      $parameters['left_field'] = $this->realField;
    }

    // Handle twig transformation.
    $twig = \Drupal::service('twig');
    $formula = $this->globalTokenReplace(
      str_replace($search, $replace, $this->getFormula()));

    try {
      $this->options['sql'] = $formula
        ? htmlspecialchars_decode($twig->createTemplate($formula)->render($parameters))
        : '';
    }
    catch (\Throwable $e) {
      \Drupal::messenger()->addError($this->t('Twig SQL error:') . ' ' .
        ($this->notAdmin() ? get_class($this) : $formula));
      $this->options['sql'] = (string) $this->t("'== Twig SQL error =='");
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   *
   * There is no need for this function to actually store the data.
   */
  public function submitGroupByForm(&$form, FormStateInterface $form_state) {
    parent::submitGroupByForm($form, $form_state);
    $item = &$form_state->get('handler')->options;

    $item['my_group_type'] = $form_state->getValue(['options', 'my_group_type']);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormula() {
    return $this->options['formula'];
  }

}
