<?php

namespace Drupal\views_sql_twig_fields\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views_sql_twig_fields\TwigSqlFields;

/**
 * Field handler which allows comparing two field values.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_sql_twig_fields_field")
 */
class ViewsSqlTwigFieldsField extends FieldPluginBase {
  use TwigSqlFields;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $this->twigSqlDefineOptions($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->displayHandler->getOption('group_by')) {
      $params['function'] = $this->options['my_group_type'];
    }
    else {
      $params = [];
    }

    $this->regenerateSqlQuery();

    $sql = $this->options['sql'] ? $this->options['sql'] : 1;
    $this->field_alias = $this->query->addField(NULL, $sql, $this->options['id'], $params);
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return $this->getValue($values);
  }

}
