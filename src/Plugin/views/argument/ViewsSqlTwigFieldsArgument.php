<?php

namespace Drupal\views_sql_twig_fields\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views_sql_twig_fields\TwigSqlFields;
use Drupal\Core\Form\FormStateInterface;

/**
 * Twig SQL argumnent handler.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("views_sql_twig_fields_argument")
 */
class ViewsSqlTwigFieldsArgument extends ArgumentPluginBase {
  use TwigSqlFields;

  /**
   * {@inheritdoc}
   */
  protected function defineSqlOptions() {
    return array_merge(parent::defineOptions(), $this->addTwigSqlOptions());
  }

  /**
   * Build the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsform($form, $form_state);

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $argument = $this->argument;
    $this->regenerateSqlQuery([
      'argument' => $argument,
      'isnumeric' => is_numeric($argument),
    ]);

    // Do simple replace in case SQL was not regenerated above.
    if ($this->options['sql']) {
      $this->query->addWhereExpression(0, $this->options['sql']);
    }
  }

}
