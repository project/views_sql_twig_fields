<?php

namespace Drupal\views_sql_twig_fields\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views_sql_twig_fields\TwigSqlFields;

/**
 * Twig SQL filter handler.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("views_sql_twig_fields_filter")
 */
class ViewsSqlTwigFieldsFilter extends FilterPluginBase {
  use TwigSqlFields;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $this->twigSqlDefineOptions($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsform($form, $form_state);

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->displayHandler->getOption('group_by')) {
      $params['function'] = $this->options['my_group_type'];
    }
    else {
      $params = [];
    }

    $this->regenerateSqlQuery();

    $this->query->addWhereExpression($this->options['group'], $this->options['sql']);
  }

}
