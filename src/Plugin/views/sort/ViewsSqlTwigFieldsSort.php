<?php

namespace Drupal\views_sql_twig_fields\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\views_sql_twig_fields\TwigSqlFields;

/**
 * SQL Twig sort handler .
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("views_sql_twig_fields_sort")
 */
class ViewsSqlTwigFieldsSort extends SortPluginBase {
  use TwigSqlFields;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $this->twigSqlDefineOptions($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsform($form, $form_state);

    $this->twigSqlBuildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->displayHandler->getOption('group_by')) {
      $params['function'] = $this->options['my_group_type'];
    }
    else {
      $params = [];
    }

    $this->regenerateSqlQuery();

    if ($this->options['sql']) {
      $this->query->addOrderBy(NULL, $this->options['sql'], $this->options['order'], $this->options['id']);
    }
  }

}
